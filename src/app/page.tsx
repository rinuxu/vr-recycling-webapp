// page.tsx
"use client";
import { Inter } from "next/font/google";
import { useEffect, useState } from "react";
import FileUploader from "./components/FileUploader";
import FolderTree from "./components/FolderTree";
import styles from "./page.module.scss";

const inter = Inter({ subsets: ["latin"] });

export type Files = {
  name: string;
  type: "image" | "model" | "folder";
  items: Files[];
};

export default function Home() {
  // This is already in the JSON file,
  // but I have to to this or it gives an error
  // saying "json is not defined".
  // I cant use fetchFilesData here either because it is
  // async
  const [files, setFiles] = useState<Files>({
    name: "Public",
    type: "folder",
    items: [],
  });

  useEffect(() => {
    fetchFilesData();
  }, []);

  const fetchFilesData = async () => {
    try {
      const response = await fetch("/api/files", { method: "GET" });
      if (response.ok) {
        const data: Files = await response.json();
        setFiles(data);
      } else {
        throw new Error("Failed to fetch files data");
      }
    } catch (error) {
      throw new Error(`Error fetching files data: ${error}`);
    }
  };
  return (
    <main>
      <div className={`${styles.pageContainer}`}>
        <div className={`${styles.container} ${inter.className}`}>
          <h1>Image & Model uploader</h1>
          <form>
            <div>
              <FileUploader updateFiles={fetchFilesData} />
            </div>
          </form>
        </div>
        <div className={`${styles.container} ${inter.className}`}>
          <h1>Files uploaded</h1>
          <form>
            <FolderTree json={files} updateFiles={fetchFilesData} />
          </form>
        </div>
      </div>
    </main>
  );
}
