// random/route.ts
import { readFile, readdir } from "fs/promises";
import { NextRequest, NextResponse } from "next/server";
import { basename, join } from "path";

function extractLabel(filename: string): string {
  const parts = filename.split("."); // Split by extension
  const namePart = parts.slice(0, parts.length - 1).join(".");
  const labelParts = namePart.split("-"); // Split by dashes
  // No label
  if (labelParts.length == 1) {
    return "";
  }
  // Assuming the label is in the last segment before the extension
  const label = labelParts[labelParts.length - 1];

  return label;
}

async function getFileContent(filePath: string) {
  try {
    const content = await readFile(filePath); // Read the file content
    return content;
  } catch (error) {
    console.error("Error while reading file\n", error);
    return null; // Return null if file reading fails
  }
}

async function getFiles() {
  let uploadedFiles: { images: string[]; models: string[] } = {
    images: [],
    models: [],
  };
  // Retrieve list of uploaded files
  try {
    const images = await readdir(join(process.cwd(), "public/uploads/images"));
    const models = await readdir(join(process.cwd(), "public/uploads/models"));
    uploadedFiles = { images: images, models: models };
  } catch (error) {
    console.error("Error while retrieving uploaded files\n", error);
  }

  return uploadedFiles;
}

export async function GET(req: NextRequest) {
  const type = req.nextUrl.searchParams.get("type");

  if (type !== "image" && type !== "model") {
    return NextResponse.json({ error: "Invalid file type." }, { status: 400 });
  }
  const uploadedFiles = await getFiles();
  let filesOfType: string[] = [];
  if (type === "image") {
    filesOfType = uploadedFiles.images.map((filename) =>
      join("public/uploads/images/", filename)
    );
  } else {
    filesOfType = uploadedFiles.models.map((filename) =>
      join("public/uploads/models/", filename)
    );
  }

  const randomFile =
    filesOfType[Math.floor(Math.random() * filesOfType.length)];
  // Read the file content
  const fileContent = await getFileContent(randomFile);
  let fileContentstr = "";

  if (fileContent !== null) {
    // TODO:
    if (type === "image") {
      fileContentstr = Buffer.from(fileContent).toString("base64");
    } else {
      fileContentstr = fileContent.toString();
    }
    const label = extractLabel(basename(randomFile));
    // Return the file content
    return NextResponse.json({
      content: fileContentstr,
      label: label,
      status: 200,
    });
  } else {
    return NextResponse.json(
      { error: "Error reading file content." },
      { status: 500 }
    );
  }
}
