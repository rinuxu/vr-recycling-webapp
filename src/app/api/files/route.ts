import * as fs from "fs";
import { NextRequest, NextResponse } from "next/server";
import { join } from "path";

const JSON_PATH: string = join(
  process.cwd(),
  "src",
  "app",
  "data",
  "files.json"
);

export async function GET(request: NextRequest) {
  // I have to use the request object or this will not
  // work on render for some reason
  const request_text = await request.text();
  console.log(request_text);
  try {
    var json = JSON.parse(fs.readFileSync(JSON_PATH).toString());
    return NextResponse.json(json);
  } catch (error) {
    console.error("Error loading files:", error);
    return NextResponse.json(
      { error: "Failed to load files" },
      { status: 500 }
    );
  }
}
