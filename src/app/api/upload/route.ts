import { Files } from "@/app/page";
import * as fs from "fs";
import { mkdir, stat, writeFile } from "fs/promises";
import mime from "mime";
import { NextRequest, NextResponse } from "next/server";
import { join } from "path";

const JSON_PATH: string = join(
  process.cwd(),
  "src",
  "app",
  "data",
  "files.json"
);

async function updateJsonFile(fileType: string, fileName: string) {
  try {
    // Read the JSON file
    var files = JSON.parse(fs.readFileSync(JSON_PATH).toString());

    // Find the index of the file type to update or create if not exists
    let fileTypeIndex = files.items.findIndex(
      (item: Files) => item.name === fileType + "s"
    );
    // Add the new file entry
    files.items[fileTypeIndex].items.push({
      name: fileName,
      type: fileType,
      // TODO: Fix this
      items: [],
    });

    // Write the updated JSON back to the file
    await writeFile(JSON_PATH, JSON.stringify(files, null, 2));
  } catch (error) {
    console.error("Error updating JSON file:", error);
  }
}

export async function POST(request: NextRequest) {
  const formData = await request.formData();

  const file = formData.get("file") as File | null;
  if (!file) {
    return NextResponse.json(
      { error: "File blob is required." },
      { status: 400 }
    );
  }

  const type = formData.get("type") as string;

  if (type !== "image" && type !== "model") {
    return NextResponse.json({ error: "Invalid file type." }, { status: 400 });
  }

  const label = formData.get("label") as string;

  const buffer = Buffer.from(await file.arrayBuffer());
  const uploadDir = join(process.cwd(), `public/uploads/${type}s`);

  try {
    await stat(uploadDir);
  } catch (e: any) {
    if (e.code === "ENOENT") {
      await mkdir(uploadDir, { recursive: true });
    } else {
      console.error(
        "Error while trying to create directory when uploading a file\n",
        e
      );
      return NextResponse.json(
        { error: "Something went wrong." },
        { status: 500 }
      );
    }
  }

  try {
    let filename = "";
    if (label === "") {
      filename = `${file.name.replace(/\.[^/.]+$/, "")}.${mime.getExtension(
        file.type
      )}`;
    } else {
      filename = `${file.name.replace(
        /\.[^/.]+$/,
        ""
      )}-${label}.${mime.getExtension(file.type)}`;
    }

    await writeFile(`${uploadDir}/${filename}`, buffer);
    // Update JSON file with the new entry
    await updateJsonFile(type, filename);
    return NextResponse.json({ fileUrl: `/uploads/${type}s/${filename}` });
  } catch (e) {
    console.error("Error while trying to upload a file\n", e);
    return NextResponse.json(
      { error: "Something went wrong." },
      { status: 500 }
    );
  }
}
