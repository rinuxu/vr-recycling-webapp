import * as fs from "fs";
import { unlink } from "fs/promises";
import { NextRequest, NextResponse } from "next/server";

const JSON_PATH: string = "./src/app/data/files.json";

export async function DELETE(request: NextRequest) {
  // Get the file name, url and type (model or image)
  const formData = await request.formData();
  const fileUrl = formData.get("file") as string;
  const fileType = formData.get("fileType") as string;
  const parts: string[] = fileUrl.split("/");
  const fileName: string = parts[parts.length - 1];

  // Load the JSON
  var files = JSON.parse(fs.readFileSync(JSON_PATH).toString());

  try {
    // Delete the file from the server
    await unlink(fileUrl.replace("/uploads", "public/uploads"));
    // Remove the deleted file from the JSON file
    // Find the index of the file to delete
    const fileTypeIndex = files.items.findIndex(
      (item: any) => item.name === fileType + "s"
    );
    const fileIndex = files.items[fileTypeIndex].items.findIndex(
      (item: any) => item.name === fileName
    );
    // Remove the file from the JSON object
    files.items[fileTypeIndex].items.splice(fileIndex, 1);

    // Write the updated JSON back to the file
    fs.writeFileSync(JSON_PATH, JSON.stringify(files, null, 2));

    return NextResponse.json({ message: "File deleted successfully" });
  } catch (error) {
    console.error("Error deleting file:", error);
    return NextResponse.json(
      { error: "Failed to delete file" },
      { status: 500 }
    );
  }
}
