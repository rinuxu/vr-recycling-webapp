// FileUploader.tsx
"use client";
import { ChangeEvent, useState } from "react";
import styles from "./FileUploader.module.scss";

interface Props {
  updateFiles: () => void;
}

const FileUploader: React.FC<Props> = ({ updateFiles }) => {
  const [imageUrl, setImageUrl] = useState("");
  const [modelUrl, setModelUrl] = useState("");
  const [label, setLabel] = useState("");

  const onFileChange = async (e: ChangeEvent<HTMLInputElement>) => {
    setModelUrl("");
    setImageUrl("");

    const fileInput = e.target;

    if (!fileInput.files) {
      console.warn("no file was chosen");
      return;
    }

    if (!fileInput.files || fileInput.files.length === 0) {
      console.warn("files list is empty");
      return;
    }

    const file = fileInput.files[0];
    const fileType = file.type.split("/")[0]; // Remove the extension

    const formData = new FormData();
    formData.append("file", file);
    formData.append("label", label); // Append label to the form data
    formData.append("type", fileType); // Append file type to the form data

    try {
      const res = await fetch("/api/upload", {
        method: "POST",
        body: formData,
      });

      if (!res.ok) {
        console.error("something went wrong, check your console.");
        return;
      }

      updateFiles();

      const data: { fileUrl: string } = await res.json();

      if (fileType === "image") {
        setImageUrl(data.fileUrl);
      } else if (fileType === "model") {
        setModelUrl(data.fileUrl);
      }
    } catch (error) {
      console.error("something went wrong, check your console.");
    }

    /** Reset file input */
    e.target.type = "text";
    e.target.type = "file";
  };

  const handleLabelChange = (e: ChangeEvent<HTMLInputElement>) => {
    setLabel(e.target.value);
  };

  return (
    <div className={styles.container}>
      <div className={styles.uploadSection}>
        <label htmlFor="file-upload" className={styles.uploadLabel}>
          Choose an model or a file:
        </label>
        <div className={styles.input_container}>
          <input
            id="file-upload"
            type="file"
            onChange={(e) => onFileChange(e)}
            accept="image/*,.obj"
            className={styles.uploadInput}
          />
        </div>
      </div>
      <div className={styles.labelSection}>
        <label htmlFor="label" className={styles.label}>
          Label:
        </label>
        <input
          id="label"
          type="text"
          value={label}
          onChange={handleLabelChange}
          className={styles.labelInput}
        />
      </div>
      {modelUrl && (
        <div className={styles.fileUploaded}>
          <p>Model uploaded to {modelUrl}</p>
        </div>
      )}
      {imageUrl && (
        <div className={styles.fileUploaded}>
          <p>Image uploaded to {imageUrl} </p>
        </div>
      )}
    </div>
  );
};

export default FileUploader;
