//FolderTree.tsx
"use client";
import { useState } from "react";
import { FcEmptyTrash } from "react-icons/fc";
import Styled from "styled-components";
import { Files } from "../page";
import RowContainer from "./RowContainer";

const Delete = Styled.div`
    margin-left: 2rem;
`;

const Container = Styled.div`
    padding:0rem 1.5rem;
`;

interface Props {
  json: Files;
  updateFiles: () => void;
}

const FolderTree: React.FC<Props> = ({ json, updateFiles }) => {
  const [expand, setExpand] = useState(false);

  const handleClick = () => {
    setExpand(!expand);
  };

  const deleteFile = async (fileName: string, fileType: string) => {
    try {
      let fileURL: string = "";
      // "model(s)" or "image(s)"
      fileURL = "/uploads/" + fileType + "s/" + fileName;
      const formData = new FormData();
      formData.append("file", fileURL);
      formData.append("fileType", fileType);
      const res = await fetch("/api/delete", {
        method: "DELETE",
        body: formData,
      });
      if (res.ok) {
        updateFiles();
      } else {
        console.error("Failed to delete file");
      }
    } catch (error) {
      console.error("Error deleting file:", error);
    }
  };

  if (json.type === "folder") {
    return (
      <Container>
        <RowContainer
          type={"folder"}
          name={json.name}
          handleClick={handleClick}
        />

        <div style={{ display: expand ? "block" : "none" }}>
          {json.items.map((item) => {
            return (
              <FolderTree
                key={item.name}
                json={item}
                updateFiles={updateFiles}
              />
            );
          })}
        </div>
      </Container>
    );
  } else {
    return (
      <Container style={{ display: "flex", alignItems: "center" }}>
        <RowContainer type={"file"} name={json.name} handleClick={() => {}} />
        <Delete>
          <FcEmptyTrash
            style={{ cursor: "pointer" }}
            onClick={() => {
              deleteFile(json.name, json.type);
            }}
          />
        </Delete>
      </Container>
    );
  }
};

export default FolderTree;
