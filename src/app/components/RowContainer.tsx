import { FcFile, FcFolder } from "react-icons/fc";
import Styled from "styled-components";

const Row = Styled.div<{type: string}>`
    display:flex;
    align-items: center;
    cursor:${({ type }) => (type === "folder" ? "pointer" : null)};
    padding: 0rem;
    margin: 0rem;height: 2rem;
`;

const Name = Styled.h5`
    margin-left: 0.5rem;
`;

interface Props {
  type: string,
  name: string,
  handleClick: (() => void);
}

const RowContainer: React.FC<Props> = ({ type, name, handleClick }) => {
  return (
    <Row type={type} onClick={handleClick}>
      {type === "folder" ? <FcFolder /> : <FcFile />}
      <Name>{name}</Name>
    </Row>
  );
};

export default RowContainer;
